package danieltribeiro.timecode.spark;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import danieltribeiro.timecode.Timecode;

import static spark.Spark.port;

public class App {

    public static Gson gson;

    public static Gson getGson() {
      if (gson == null) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Timecode.class, new TimecodeSerializer());
        gson = gsonBuilder.create(); 
      }
      return gson;
    }
    public static void main(String[] args) {
      String portNumber = System.getenv("PORT");
      if (portNumber == null) {
          portNumber = "8080";
      }
      port(Integer.valueOf(portNumber));

      new ConvertController();
    }
}
