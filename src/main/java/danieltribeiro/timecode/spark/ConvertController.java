package danieltribeiro.timecode.spark;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.gson.Gson;

import danieltribeiro.timecode.Timecode;

import static spark.Spark.get;
import static spark.Spark.post;

public class ConvertController  {


  public ConvertController() {
    Gson gson = App.getGson();
    ConvertController convertController = this;

    get("/detail/:srcfr/:value", (req, res) -> {
        return convertController.detail(req.params("srcfr"), req.params("value"));
    }, gson::toJson);

    get("/add/:srcfr/:value/:value2", (req, res) -> {
        return convertController.add(req.params("srcfr"), req.params("value"), req.params("value2"));
    }, gson::toJson);

    get("/convert/:srcfr/:value/:destfr/:offset", (req, res) -> {
      return convertController.convertSingle(req.params("srcfr"), req.params("value"), req.params("destfr"), req.params("offset"));
    }, gson::toJson);

    post("/convert/:srcfr/:destfr/:offset", (req, res) -> {
      @SuppressWarnings("unchecked")
      List<Object> list = gson.fromJson(req.body(), List.class);
      return convertController.convertMany(req.params("srcfr"), req.params("destfr"), req.params("offset"), list);
    }, gson::toJson);

    post("/convert/:srcfr/:destfr/:offset/asArray", (req, res) -> {
      @SuppressWarnings("unchecked")
      List<Object> list = gson.fromJson(req.body(), List.class);
      return convertController.convertManyAsArray(req.params("srcfr"), req.params("destfr"), req.params("offset"), list);
    }, gson::toJson);


    post("/convert/:srcfr/:destfr/:offset/asMap", (req, res) -> {
      @SuppressWarnings("unchecked")
      List<Object> list = gson.fromJson(req.body(), List.class);
      return convertController.convertManyAsMap(req.params("srcfr"), req.params("destfr"), req.params("offset"), list);
    }, gson::toJson);
  }

  public Timecode detail(String srcfr, String value) {
    return Timecode.parse(value, srcfr);
  }

  public Timecode add(String srcfr, String value, String value2) {
    return Timecode.parse(value, srcfr).add(Timecode.parse(value2, srcfr));
  }

  public Timecode convertSingle(String srcfr, String value, String destfr, String offset) {
    return Timecode.parse(value, srcfr).convertFrameRate(destfr, offset);
  }

  public List<TimecodeConverted> convertManyAsArray(String srcfr, String destfr, String offset, List<Object> list) {
    List<TimecodeConverted> newList = new ArrayList<>();
    for (Object s : list) {
      Timecode tc = Timecode.parse(s.toString(), srcfr);
      newList.add(new TimecodeConverted().setSrcTimecode(tc).setDestTimecode(tc.convertFrameRate(destfr, offset)));
    };
    return newList;
  }


  public List<Timecode> convertMany(String srcfr, String destfr, String offset, List<Object> list) {
    List<Timecode> newList = new ArrayList<>();
    for (Object s : list) {
      newList.add(Timecode.parse(s.toString(), srcfr).convertFrameRate(destfr, offset));
    };
    return newList;
  }


  public Map<String, Timecode> convertManyAsMap(String srcfr, String destfr, String offset, List<Object> list) {
    return (Map<String, Timecode>) list.parallelStream().distinct().collect(Collectors.toMap(s -> s.toString(), s -> Timecode.parse(s.toString(), srcfr).convertFrameRate(destfr, offset)));
  }
}