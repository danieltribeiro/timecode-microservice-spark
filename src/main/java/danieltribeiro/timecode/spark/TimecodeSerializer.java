package danieltribeiro.timecode.spark;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import danieltribeiro.timecode.Timecode;

public class TimecodeSerializer implements JsonSerializer<Timecode> {

  private DateFormat timestampFormat = new SimpleDateFormat("HH:mm:ss.SSS");
  {
    timestampFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
  }

  @Override
  public JsonElement serialize(Timecode src, java.lang.reflect.Type typeOfSrc, JsonSerializationContext context) {
    JsonObject json = new JsonObject();

    json.addProperty("string", src.toString());
    json.addProperty("milliseconds", src.getTotalMilis());
    json.addProperty("frames", src.getTotalFrames());
    json.addProperty("timestamp", formatAsTimestamp(src.getTotalMilis()));
    json.addProperty("frameRate", src.getFramerate().getName());

    return json;
  }

  private String formatAsTimestamp(int milis) {
    return timestampFormat.format(new Date(milis));
  }
}