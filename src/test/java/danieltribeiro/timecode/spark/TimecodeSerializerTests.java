package danieltribeiro.timecode.spark;

import static org.junit.Assert.assertEquals;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.junit.Before;
import org.junit.Test;

import danieltribeiro.timecode.Timecode;

public class TimecodeSerializerTests {
  
  private Gson gson;

  @Before
  public void before() {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.registerTypeAdapter(Timecode.class, new TimecodeSerializer());
    gson = gsonBuilder.create(); 
  }

  @Test public void testConvertToJson() {
    Timecode tc = Timecode.parse("01:00:00:00", "29.97");
    JsonElement e =  new JsonParser().parse(gson.toJson(tc));
    JsonObject o = e.getAsJsonObject();
    
    assertEquals("01:00:00:00", o.get("string").getAsString());
    assertEquals(3600000, o.get("milliseconds").getAsInt());
    assertEquals(107892, o.get("frames").getAsInt());
  }
}