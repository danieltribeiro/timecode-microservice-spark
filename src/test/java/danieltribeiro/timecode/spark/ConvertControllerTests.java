/**
 * The test class
 *
 */
package danieltribeiro.timecode.spark;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import com.despegar.http.client.GetMethod;
import com.despegar.http.client.HttpResponse;
import com.despegar.http.client.PostMethod;
import com.despegar.sparkjava.test.SparkServer;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.internal.LinkedTreeMap;

import org.junit.ClassRule;
import org.junit.Test;

import spark.servlet.SparkApplication;

import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;

public class ConvertControllerTests {

	public static class ConvertContollerTestsSparkApplication implements SparkApplication {
		@Override
		public void init() {
			new ConvertController();
		}
	}

	@ClassRule
	public static SparkServer<ConvertContollerTestsSparkApplication> testServer = new SparkServer<>(ConvertControllerTests.ConvertContollerTestsSparkApplication.class, 4567);

	@Test
	public void testSingleConvert() throws Exception {
		/* The second parameter indicates whether redirects must be followed or not */
		GetMethod get = testServer.get("/convert/30.00/02:00:00:00/29.97/01:00:00:00", false);
		get.addHeader("Accept", "application/json");
		HttpResponse httpResponse = testServer.execute(get);
    assertEquals(200, httpResponse.code());
    JsonElement rootNode = new JsonParser().parse(new String(httpResponse.body()));
    JsonObject jsonObject = rootNode.getAsJsonObject();
 		assertEquals("02:00:03:18", jsonObject.get("string").getAsString());
	}

  
	@Test public void testMultipleConvert() throws Exception {
    /* The second parameter indicates whether redirects must be followed or not */
    Object[] values = new String[]{"108000f", "01:00:00:00", "01:00:00.000"};
    String body = new Gson().toJson(values);
    PostMethod post = testServer.post("/convert/30.00/29.97/0", body, false);
		post.addHeader("Content-Type", "application/json");
		post.addHeader("Accept", "application/json");
		HttpResponse httpResponse = testServer.execute(post);
    assertEquals(200, httpResponse.code());
    JsonElement rootNode = new JsonParser().parse(new String(httpResponse.body()));
    JsonArray jsonArray = rootNode.getAsJsonArray();
    assertEquals(jsonArray.size(), values.length);
    for (int i = 0; i < values.length; i++) {
      assertEquals("01:00:03:18", jsonArray.get(i).getAsJsonObject().get("string").getAsString());
    }
	}

  @Test public void testMultipleConvertAsArray() throws Exception {
    /* The second parameter indicates whether redirects must be followed or not */
    Object[] values = new String[]{"108000f", "01:00:00:00", "01:00:00.000"};
    String body = new Gson().toJson(values);
    PostMethod post = testServer.post("/convert/30.00/29.97/0/asArray", body, false);
		post.addHeader("Content-Type", "application/json");
		post.addHeader("Accept", "application/json");
		HttpResponse httpResponse = testServer.execute(post);
    assertEquals(200, httpResponse.code());
    JsonElement rootNode = new JsonParser().parse(new String(httpResponse.body()));
    JsonArray jsonArray = rootNode.getAsJsonArray();
    assertEquals(jsonArray.size(), values.length);
    for (int i = 0; i < values.length; i++) {
      assertEquals("01:00:00:00", jsonArray.get(i).getAsJsonObject().get("srcTimecode").getAsJsonObject().get("string").getAsString());
      assertEquals("01:00:03:18", jsonArray.get(i).getAsJsonObject().get("destTimecode").getAsJsonObject().get("string").getAsString());
    }
	}

  @Test public void testMultipleConvertAsMap() throws Exception {
    /* The second parameter indicates whether redirects must be followed or not */
    Object[] values = new String[]{"108000f", "01:00:00:00", "01:00:00.000"};
    String body = new Gson().toJson(values);
    PostMethod post = testServer.post("/convert/30.00/29.97/0/asMap", body, false);
		post.addHeader("Content-Type", "application/json");
		post.addHeader("Accept", "application/json");
		HttpResponse httpResponse = testServer.execute(post);
    assertEquals(200, httpResponse.code());
    Type type = new TypeToken<Map<String, LinkedTreeMap<String, Object>>>(){}.getType();
    Map<String, LinkedTreeMap<String, Object>> map = new Gson().fromJson(new String(httpResponse.body()), type);
    assertEquals(values.length, map.size());
    for (int i = 0; i < values.length; i++) {
      LinkedTreeMap<String, Object> o = map.get(values[i]);
      String string = o.get("string").toString();
      assertEquals("01:00:03:18", string);
    }
  }


}